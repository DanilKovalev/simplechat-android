package co.intentservice.chatui.views;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.R;

/**
 * View to display the messages that have been sent through the chat-ui.
 *
 * Created by James Lendrem
 */
public class ItemSentView extends MessageView {

    private CardView bubble;
    private TextView messageTextView, timestampTextView;
    public ImageView imageView;
    private ChatView.OnAvatarClickListener listener;

    public void setImage(String id) {
        if (id.equals("video")) {
            imageView.setVisibility(View.VISIBLE);
            imageView.setImageResource(R.drawable.play_icon);
            imageView.getLayoutParams().width= ViewGroup.LayoutParams.WRAP_CONTENT;
        } else if (!id.isEmpty()) {
            imageView.setVisibility(View.VISIBLE);
            Glide.with(getContext()).load(id).into(imageView);
        } else {
            imageView.setVisibility(View.GONE);
            imageView.setImageDrawable(null);
        }
    }

    @Override
    public void clickOnAvatar(ChatView.OnAvatarClickListener listener) {
        this.listener = listener;
    }

    public void setAvatar(final String url) {
        final ImageView avatarView = (ImageView) findViewById(R.id.sentAvatarView);
        System.out.println("KUN POK: " + url);

        if (url.equals("http://www.sheffield.com/wp-content/uploads/2013/06/placeholder.png")) {
        } else {

            ActivityManager am = (ActivityManager)getContext().getSystemService(Context.ACTIVITY_SERVICE);
            ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
            if (cn != null) {
                Glide
                        .with(getContext())
                        .load(url) // the uri you got from Firebase
                        .apply(RequestOptions.circleCropTransform())
                        .into(avatarView); //Your imageView variable
            }
            avatarView.setOnClickListener(new View.OnClickListener() {
                //@Override
                public void onClick(View v) {
                    listener.clickOnAvatar(url);
                }
            });
        }
    }

    public void setMessage(String message) {
        if (messageTextView == null) {
            messageTextView = (TextView) findViewById(R.id.message_text_view);
        }

        messageTextView.setText(message);
    }

    /**
     * Method to set the timestamp that the message was received or sent on the screen.
     * @param timestamp The timestamp that you want to be displayed.
     */
    public void setTimestamp(String timestamp) {

        if (timestampTextView == null) {

            timestampTextView = (TextView) findViewById(R.id.timestamp_text_view);

        }

        timestampTextView.setText(timestamp);

    }

    /**
     * Method to set the background color that you want to use in your message.
     * @param background The background that you want to be displayed.
     */
    public void setBackground(@ColorInt int background) {

        if (bubble == null) {

            this.bubble = (CardView) findViewById(R.id.bubble);

        }

        bubble.setCardBackgroundColor(background);

    }

    /**
     * Method to set the elevation of the view.
     * @param elevation The elevation that you want the view to be displayed at.
     */
    public void setElevation(float elevation) {

        if (bubble == null) {

            this.bubble = (CardView) findViewById(R.id.bubble);

        }

        bubble.setCardElevation(elevation);

    }

    /**
     * Constructs a new message view.
     * @param context
     */
    public ItemSentView(Context context) {

        super(context);
        initializeView(context);

    }

    /**
     * Constructs a new message view with attributes, this constructor is used when we create a
     * message view using XML.
     * @param context
     * @param attrs
     */
    public ItemSentView(Context context, AttributeSet attrs) {

        super(context, attrs);
        initializeView(context);

    }

    /**
     * Inflates the view so it can be displayed and grabs any child views that we may require
     * later on.
     * @param context   The context that is used to inflate the view.
     */
    private void initializeView(Context context) {

        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.chat_item_sent, this);

        this.bubble = (CardView) findViewById(R.id.bubble);
        this.messageTextView = (TextView) findViewById(R.id.message_text_view);
        this.timestampTextView = (TextView) findViewById(R.id.timestamp_text_view);
        this.imageView = (ImageView) findViewById(R.id.imageView);

        imageView.setVisibility(View.GONE);
    }

}
