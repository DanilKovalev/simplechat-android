package co.intentservice.chatui.models;

import android.net.Uri;
import android.text.format.DateFormat;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


/**
 *
 * Chat Message model used when ChatMessages are required, either to be sent or received,
 * all messages that are to be shown in the chat-ui must be contained in this model.
 *
 */
public class ChatMessage {
    private String message;
    private long timestamp;
    private Type type;
    private String imageURL = "";
    private String avatarURL = "";

    public ChatMessage(String message, long timestamp, Type type, String imageURL, String avatarURL){
        this.message = message;
        this.timestamp = timestamp;
        this.type = type;
        this.imageURL = imageURL;
        this.avatarURL = avatarURL;
    }

    public String getImage() {return imageURL;}

    public String getURL() {return avatarURL;}

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public String getFormattedTime(){

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;
        long unixSeconds = timestamp;
        Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM - hh:mm a"); // the format of your date
        long timeDifference = System.currentTimeMillis() - timestamp;

        if (timeDifference < oneDayInMillis) {
            sdf = new SimpleDateFormat("hh:mm a") ;
        }

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        sdf.setTimeZone(tz); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);

        return formattedDate;

    }

    public enum Type {
        SENT, RECEIVED
    }
}
