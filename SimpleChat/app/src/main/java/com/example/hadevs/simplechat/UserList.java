package com.example.hadevs.simplechat;
import android.app.Activity;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsoluteLayout;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.hadevs.simplechat.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UserList extends ArrayAdapter<String>{

    private final Activity context;
    private final ArrayList<String> web;
    private final ArrayList<String> subTexts;
    private final ArrayList<String> ids;

    public UserList(Activity context,
                    ArrayList<String> web, ArrayList<String> ids, ArrayList<String> subTexts) {
        super(context, R.layout.list_user, web.toArray(new String[0]));
        this.context = context;
        this.web = web;
        this.subTexts = subTexts;
        this.ids = ids;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        final View rowView= inflater.inflate(R.layout.list_user, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);

        final ImageView imageView = (ImageView) rowView.findViewById(R.id.img);
        txtTitle.setText(web.get(position));

        TextView subtextTitle = (TextView) rowView.findViewById(R.id.txt2);
        subtextTitle.setText(subTexts.get(position));

        // load url for current user
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imagesRef = storageRef.child("avatars");

        imagesRef.child(ids.get(position)).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.with(rowView.getContext()).load(uri.toString()).transform(new co.intentservice.chatui.views.CircleTransform()).fit().centerInside().into(imageView);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Picasso.with(rowView.getContext()).load("http://www.sheffield.com/wp-content/uploads/2013/06/placeholder.png").transform(new co.intentservice.chatui.views.CircleTransform()).fit().centerInside().into(imageView);
            }
        });
        return rowView;
    }
}