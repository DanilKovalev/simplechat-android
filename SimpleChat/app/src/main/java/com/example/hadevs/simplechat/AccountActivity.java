package com.example.hadevs.simplechat;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.TimeZone;

import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;

public class AccountActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);

        addPersonalAccountButtonListener();
        addOffertaButtonListener();
        setTitle("Личный кабинет");

        EditText timePassField = (EditText) findViewById(R.id.passTimeField);
        Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd.MM.yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            timePassField.setText(sdf.format(myCalendar.getTime()));
        };

        Button payButton = findViewById(R.id.payButton);
        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(AccountActivity.this);

                long unitExperiedLong = MyStorage.currentUser.expiriedDateUnix;
                Date date = new Date(unitExperiedLong * 1000L);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
                dateFormat.setTimeZone(TimeZone.getTimeZone("GMT+3"));
                String formattedDate = dateFormat.format(date);

                builder.setTitle("Оплата")
                        .setMessage("Ваш аккаунт оплачен до: " + formattedDate)
                        .setCancelable(true)
                        .setNegativeButton("ОК",
                                (dialog, id) -> dialog.cancel());
                AlertDialog alert = builder.create();
                alert.show();
            }
        });

        final Button fiasButton = findViewById(R.id.button7);
        fiasButton.setOnClickListener(view -> {
            Intent intent = new Intent(AccountActivity.this, FIASActivity.class);
            startActivity(intent);
        });

        final EditText passNumberField = (EditText) findViewById(R.id.passNumberField);
        passNumberField.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length()-passNumberField.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {
                    if (phone.length() >= 4 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = phone.substring(0, 4) + " " + phone.substring(4);
                        passNumberField.setText(ans);
                        passNumberField.setSelection(passNumberField.getText().length()-cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });


        final Button button = findViewById(R.id.saveButton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                saveButtonClicked();
            }
        });

        final Button button4 = findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent newIntent = new Intent(AccountActivity.this, ChangePasswordActivity.class);
                startActivity(newIntent);
            }
        });
    }


    private void addOffertaButtonListener() {
        Button dogovorButton = findViewById(R.id.dogovorButton);
        dogovorButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AccountActivity.this, OffertaActivity.class));
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void fetchAndFillData() {
        final EditText surnameField = findViewById(R.id.surameField);
        surnameField.setText(MyStorage.currentUser.surname.replace("null", ""));

        final EditText nameField = findViewById(R.id.nameField);
        nameField.setText(MyStorage.currentUser.name.replace("null", ""));

        final EditText secondNameField = findViewById(R.id.secondNameField);
        secondNameField.setText(MyStorage.currentUser.secondName.replace("null", ""));

        final EditText passNumberField = findViewById(R.id.passNumberField);
        String ps = MyStorage.currentUser.passNumber.replace("null", "");
        if (ps.equals(" ")) {
            ps = "";
        }
        passNumberField.setText(ps);

        final EditText facePassportField = findViewById(R.id.facePassField);
        facePassportField.setText(MyStorage.currentUser.facePasport.replace("null", ""));

        final EditText timePassField = findViewById(R.id.passTimeField);
        timePassField.setText(MyStorage.currentUser.timePasport.replace("null", ""));

        final EditText birthdayField = findViewById(R.id.birthDayField);
        birthdayField.setText(MyStorage.currentUser.birthday.replace("null", ""));

        final EditText innField = findViewById(R.id.innField);
        innField.setText(MyStorage.currentUser.INN.replace("null", ""));

        final EditText registrationField = findViewById(R.id.registrationField);
        String separator = ", ";
        if (MyStorage.currentUser.fiasStreetNumber != null && !MyStorage.currentUser.fiasStreetNumber.isEmpty() && !MyStorage.currentUser.fiasStreetNumber.equals("null")) {
            registrationField.setText(MyStorage.currentUser.fiasIndex + separator + MyStorage.currentUser.fiasOblast + separator + MyStorage.currentUser.fiasCity + separator + MyStorage.currentUser.fiasStreet + separator + MyStorage.currentUser.fiasStreetNumber + separator + MyStorage.currentUser.fiasApartament);
        } else {
            registrationField.setText("ФИАС не заполнен полностью.");
        }

        registrationField.setEnabled(false);

        setDatePickerFor(timePassField);
        setDatePickerFor(birthdayField);

        final EditText codeSubdivisionField = findViewById(R.id.codeSubdivisionField);
        codeSubdivisionField.setText(MyStorage.currentUser.codeSubdivision.replace("null", ""));
    }

    @Override
    protected void onResume() {
        super.onResume();

        fetchAndFillData();
    }

    private void addPersonalAccountButtonListener() {
        Button personalAccountButton = findViewById(R.id.addPunctButton);
        personalAccountButton.setOnClickListener(view -> startActivity(new Intent(AccountActivity.this, PersonalAccount.class)));
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setDatePickerFor(final EditText editText) {
        Calendar myCalendar = Calendar.getInstance();

        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            String myFormat = "dd.MM.yyyy"; //In which you need put here
            SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

            editText.setText(sdf.format(myCalendar.getTime()));
        };

        editText.setInputType(0);
   
    editText.setOnTouchListener(new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (motionEvent.getAction() == MotionEvent.ACTION_UP && !editText.hasFocus()) {
                // onClick() is not called when the EditText doesn't have focus,
                // onFocusChange() is called instead, which might have a different
                // meaning. This condition calls onClick() when click was performed
                // but wasn't reported. Condition can be extended for v.isClickable()
                // or v.isEnabled() if needed. Returning false means that everything
                // else behaves as before.
                editText.performClick();

                new DatePickerDialog(AccountActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
            return false;
        }
    });

    }

    private void saveButtonClicked() {
        final EditText surnameField = findViewById(R.id.surameField);
        final String   surname = surnameField.getText().toString();

        final EditText nameField = findViewById(R.id.nameField);
        final String   name = nameField.getText().toString();

        final EditText secondNameField = findViewById(R.id.secondNameField);
        final String   secondName = secondNameField.getText().toString();

        final EditText passNumberField = findViewById(R.id.passNumberField);
        final String   passnumber = passNumberField.getText().toString();

        final EditText facePassportField = findViewById(R.id.facePassField);
        final String   facePassport = facePassportField.getText().toString();

        final EditText timePassField = findViewById(R.id.passTimeField);
        final String   timePassport = timePassField.getText().toString();

        final EditText birthdayField = findViewById(R.id.birthDayField);
        final String   birthday = birthdayField.getText().toString();

        final EditText innField = findViewById(R.id.innField);
        final String   inn = innField.getText().toString();

        final EditText registrationField = findViewById(R.id.registrationField);
        final String   registration = registrationField.getText().toString();

        final EditText codeSubdivisionField = findViewById(R.id.codeSubdivisionField);
        final String   codeSubdivision = codeSubdivisionField.getText().toString();

        if (surname.isEmpty() || name.isEmpty() || secondName.isEmpty() || passnumber.isEmpty() || facePassport.isEmpty() || timePassport.isEmpty() || birthday.isEmpty() || inn.isEmpty() || registration.isEmpty() || codeSubdivision.isEmpty()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AccountActivity.this);
            builder.setTitle("Ошибка")
                    .setMessage("Заполните все поля.")
                    .setCancelable(true)
                    .setNegativeButton("ОК",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
            AlertDialog alert = builder.create();
            alert.show();
            return;
        }



        MyStorage.currentUser.name = name.replaceAll("\\s+","");
        MyStorage.currentUser.surname = surname.replaceAll("\\s+","");
        MyStorage.currentUser.secondName = secondName.replaceAll("\\s+","");
        MyStorage.currentUser.passNumber = passnumber;
        MyStorage.currentUser.facePasport = facePassport;
        MyStorage.currentUser.timePasport = timePassport;
        MyStorage.currentUser.birthday = birthday;
        MyStorage.currentUser.INN = inn;
        MyStorage.currentUser.registration = registration;
        MyStorage.currentUser.codeSubdivision = codeSubdivision;
//
//        SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
//        SharedPreferences.Editor editor = sharedPref.edit();
//        editor.putString("passNumber", passnumber);
//        editor.commit();

        MyStorage.refreshOnServer();
                AlertDialog.Builder builder = new AlertDialog.Builder(AccountActivity.this);
                String message = "Данные изменены!";

                    builder.setTitle("Готово")
                            .setMessage(message)
                            .setCancelable(true)
                            .setNegativeButton("ОК",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();

    }
}


