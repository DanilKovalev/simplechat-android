package com.example.hadevs.simplechat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;

/**
 * Created by hadevs on 29/01/2018.
 */

public class FIASManager {

    static void search(final String type, String objectId, String text, Context context, final FIASCallbackInterface callback) {


        RequestQueue queue = Volley.newRequestQueue(context);
        // configuring url
        String url = null;
        switch (type) {
            case "city":
                url = "http://kladr-api.ru/api.php?query=" + text + "&contentType=city&withParent=1&limit=1&regionId=" + objectId;
                break;
            case "streetName":
                url = "http://kladr-api.ru/api.php?query=" + text + "&contentType=street&withParent=1&limit=1&cityId=" + objectId;
                break;
            case "streetNumber":
                url = "http://kladr-api.ru/api.php?query=" + text + "&contentType=building&withParent=1&limit=1&streetId=" + objectId;
                break;
            case "oblast":
                url = "http://kladr-api.ru/api.php?query=" + text + "&contentType=region&withParent=1&limit=1";
                break;
        }

        System.out.println("MOre: " + url);

        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                response -> {
                    // display response
                    String jsonString = response.toString();
                    System.out.println("Getted array: " + jsonString);

                    try {
                        JSONObject json = new JSONObject(jsonString);
                        JSONArray result = json.getJSONArray("result");
                        if (result.length() > 0) {
                            ArrayList objects = new ArrayList();
                            for (int i = 0; i < result.length(); i++) {
                                FIASObject object = new FIASObject(result.getJSONObject(i).getString("name"), result.getJSONObject(i).getString("id"));
                                objects.add(object);
                            }

                            // configuring additional Info
                            String additionalInfo = null;
                            if (type.equals("city")) {
                                JSONArray parents = result.getJSONObject(0).getJSONArray("parents");
                                if (parents.length() > 0) {
                                    JSONObject parent = parents.getJSONObject(0);
                                    additionalInfo = parent.getString("name") + " " + parent.getString("type");
                                }
                            } else if (type.equals("streetNumber")) {
                                additionalInfo = result.getJSONObject(0).getString("zip");
                            }

                            callback.onTaskFinished(objects, additionalInfo);
                        } else {
                            callback.onTaskFinished(new ArrayList(), null);
                        }
                    } catch (JSONException e) {
                        System.out.println("FOCUSD");
                        e.printStackTrace();
                    }
                },
                error -> Log.d("Error.Response", error.toString())
        );

        getRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(getRequest);
    }

}

interface FIASCallbackInterface {
    void onTaskFinished(ArrayList<FIASObject>fiasObjects, String additionalInfo);
}

