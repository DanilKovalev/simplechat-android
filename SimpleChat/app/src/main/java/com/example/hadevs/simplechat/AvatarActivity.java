package com.example.hadevs.simplechat;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.UUID;

import co.intentservice.chatui.views.*;

public class AvatarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avatar);
        setTitle("Выбор аватара");
        setListeners();
        downloadPhoto();
        fillData();
    }

    private void fillData() {
        final TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(MyStorage.currentUser.name + " " + MyStorage.currentUser.surname);
    }

    private void downloadPhoto() {
        final ImageView imageView = (ImageView) findViewById(R.id.imageView2);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference().child("avatars/" + MyStorage.currentUser.id);
        storageRef.getDownloadUrl().addOnSuccessListener(uri -> {
            System.out.println("Gilmous: " + uri);
            Glide
                    .with(AvatarActivity.this)
                    .load(uri) // the uri you got from Firebase
                    .apply(RequestOptions.circleCropTransform())
                    .into(imageView); //Your imageView variable
        });
    }



    private void setListeners() {
        Button button= (Button) findViewById(R.id.button2);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadButtonClicked();
            }
        });
    }

    private void uploadButtonClicked() {
        AlertDialog.Builder sourceBuilder = new AlertDialog.Builder(AvatarActivity.this);
        sourceBuilder.setTitle("Приложение")
                .setMessage("Выберите метод отправки файла.")
                .setCancelable(true)

                .setNeutralButton("Отменить",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Фотография",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AvatarActivity.this);
                                builder.setTitle("Приложение")
                                        .setMessage("Выберите метод отправки фотографии.")
                                        .setCancelable(true)

                                        .setNeutralButton("Отменить",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        dialog.cancel();
                                                    }
                                                })
                                        .setPositiveButton("Фотоальбом",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                        startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
                                                        dialog.cancel();
                                                    }
                                                })
                                        .setNegativeButton("Камера",
                                                new DialogInterface.OnClickListener() {
                                                    public void onClick(DialogInterface dialog, int id) {
                                                        Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                        startActivityForResult(takePicture, 0);//zero can be replaced with any action code
                                                        dialog.cancel();
                                                    }
                                                });

                                AlertDialog alert = builder.create();
                                alert.show();
                                dialog.cancel();
                            }
                        });

        AlertDialog alert = sourceBuilder.create();
        alert.show();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Uri file = imageReturnedIntent.getData();
                    upload(file);
                }
                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri file = imageReturnedIntent.getData();
                    upload(file);
                }
                break;
            }
        }

        private void upload(Uri file) {
            final ImageView imageView = (ImageView) findViewById(R.id.imageView2);
            Picasso.with(AvatarActivity.this).load(file).transform(new co.intentservice.chatui.views.CircleTransform()).fit().centerInside().into(imageView);

            FirebaseStorage storage = FirebaseStorage.getInstance();
            StorageReference storageRef = storage.getReference();
            StorageReference riversRef = storageRef.child("avatars/" + MyStorage.currentUser.id);
            UploadTask uploadTask = riversRef.putFile(file);

            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    String downloadUrl = taskSnapshot.getDownloadUrl().toString();
//                    Picasso.with(AvatarActivity.this).load(downloadUrl).transform(new co.intentservice.chatui.views.CircleTransform()).fit().centerInside().into(imageView);
                }
            });
        }

}

class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;

    public DownloadImageTask(ImageView bmImage) {
        this.bmImage = bmImage;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}