package com.example.hadevs.simplechat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.Set;

import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;


public class SettingsActivity extends AppCompatActivity {

    String[] names = { "Личный кабинет", "Аватар", "Обои", "Уведомления", "Помощь", "Регистрация нового пользователя", "Выход"};
    Integer[] imageIds = {
            R.drawable.user,
            R.drawable.add_user,
            R.drawable.wallpaper,
            R.drawable.notifications,
            R.drawable.help,
            R.drawable.add_user,
            R.drawable.user
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        setTitle("Настройки");

        // находим список
        ListView lvMain = (ListView) findViewById(R.id.listView);

        CustomList adapter = new
                CustomList(SettingsActivity.this, names, imageIds);

        lvMain.setAdapter(adapter);
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 0) {
                    startActivity(new Intent(SettingsActivity.this, AccountActivity.class));
                } else if (position == 2) {
                    startActivity(new Intent(SettingsActivity.this, WallpapersActivity.class));
                } else if (position == 1) {
                    startActivity(new Intent(SettingsActivity.this, AvatarActivity.class));
                } else if (position == 3) {
                    startActivity(new Intent(SettingsActivity.this, NotificationsActivity.class));
                } else if (position == 5) {
                    startActivity(new Intent(SettingsActivity.this, RegisterActivity.class));
                } else if (position == 6) {
                    SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.remove("passNumber");
                    editor.commit();
                    startActivity(new Intent(SettingsActivity.this, MainActivity.class));
                }
            }
        });
        // присваиваем адаптер списку
        lvMain.setAdapter(adapter);
    }
}
