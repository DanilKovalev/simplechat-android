package com.example.hadevs.simplechat;


import java.util.HashMap;

/**
 * Created by Hadevs on 26.10.2017.
 */

final class User {
    public String id;
    public String password;
    public String name;
    public String surname;
    public String secondName;
    public String phoneNumber;

    public String token = "";
    public String placePasport = "";
    public String facePasport= "";
    public String timePasport = "";
    public String birthday = "";
    public String INN = "";
    public String registration = "";
    public String passNumber = "";


    public String bill = "";
    public String BIC = "";
    public String bankName = "";
    public String bankBill = "";
    public String bankCard = "";
    public int expiriedDateUnix = 0;

    public String fiasIndex = "";
    public String fiasOblast = "";
    public String fiasCity = "";
    public String fiasStreet = "";
    public String fiasStreetNumber = "";
    public String fiasApartament= "";
    public String fiasOblastId = "";
    public String fiasCityId = "";
    public String fiasStreetId = "";
    public String codeSubdivision = "";


    // point initialized from parameters
    public User(String id, String password, String name, String surname, String secondName, String phoneNumber) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.secondName = secondName;
        this.phoneNumber = phoneNumber;
    }

}