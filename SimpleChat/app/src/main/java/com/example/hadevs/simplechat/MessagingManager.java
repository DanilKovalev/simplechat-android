package com.example.hadevs.simplechat;

import android.app.Notification;
import android.database.CursorJoiner;
import android.os.AsyncTask;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Hadevs on 26.11.2017.
 */

public class MessagingManager {


    public static final String FCM_MESSAGE_URL = "https://fcm.googleapis.com/fcm/send";
    static OkHttpClient mClient = new OkHttpClient();
    public static void sendMessage(final JSONArray recipients, final String title, final String body, final String icon, final String message, final String token) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    JSONObject root = new JSONObject();
                    JSONObject notification = new JSONObject();
                    notification.put("body", message);
                    notification.put("title", title);
                    notification.put("icon", icon);

                    JSONObject data = new JSONObject();
                    data.put("message", message);
                    root.put("notification", notification);
                    root.put("data", data);
                    root.put("to", token);

                    String result = postToFCM(root.toString());
                    System.out.println("Result: " + result);
                    return result;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String result) {
                try {
                    JSONObject resultJson = new JSONObject(result);
                    int success, failure;
                    success = resultJson.getInt("success");
                    failure = resultJson.getInt("failure");
                    System.out.println("Message sended with success:  " + success + "and failure: " + failure + " user token " + token);

                } catch (JSONException e) {
                    e.printStackTrace();
                    System.out.println("Message Failed, Unknown error occurred.");
                }
            }
        }.execute();
    }

    static String postToFCM(String bodyString) throws IOException {

        RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), bodyString);
        Request request = new Request.Builder()
                .url(FCM_MESSAGE_URL)
                .post(body)
                .addHeader("Authorization", "key=AAAA0RYAAdY:APA91bEmTKUjxf5r1a7uu-E7SpNJypbvHQOkI8yVydLp3yFO4C199Ui9wZOyRkyHWCi6CBTs--1NDr34WfjkN9fFBv2pgXow5hp9MRr-zZX_1wnfBb9-x1wA2vbs03jQgxhSFcVvoUVz")
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = mClient.newCall(request).execute();

        return response.body().string();
    }

    public static void sendMessage(User user, String text) {
        System.out.println("German: " + user.token);
        final String[] ids = {user.token};
        try {
            final JSONArray regArray = new JSONArray(ids);
            sendMessage(regArray, user.name + " " + user.surname, "body", "icon", text, user.token);
            System.out.println("SINUS");

        } catch (JSONException e) {
            System.out.println("COSINUS");
            e.printStackTrace();
        }
    }


}
