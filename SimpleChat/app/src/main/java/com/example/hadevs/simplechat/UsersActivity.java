package com.example.hadevs.simplechat;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;

public class UsersActivity extends AppCompatActivity {
    ProgressDialog mDialog;
    ArrayList<String> localPhones = new ArrayList<String>();

    String[] accesedPhones = {

    };

    private List<String> getContactNames() {
        List<String> contacts = new ArrayList<>();
        ContentResolver cr = getContentResolver();
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                String name =
                    cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                contacts.add(name);
            } while (cursor.moveToNext());
        }
        cursor.close();

        return contacts;
    }

    final int PERMISSIONS_REQUEST_READ_CONTACTS = 83;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);

        create();

    }

    void create() {
        mDialog = new ProgressDialog(UsersActivity.this);
        mDialog.setMessage("Загрузка пользователей...");
        mDialog.setCancelable(false);
        mDialog.show();
        setTitle("Пользователи");

        if (UsersActivity.this.checkSelfPermission(android.Manifest.permission.READ_CONTACTS)
                == PackageManager.PERMISSION_GRANTED) {

            testRequest();
        }

        NotificationCenter.defaultCenter().addFucntionForNotification("permissionsUpdated", new Runnable() {
            @Override
            public void run() {
                if (UsersActivity.this.checkSelfPermission(android.Manifest.permission.READ_CONTACTS)
                        == PackageManager.PERMISSION_GRANTED) {
                    testRequest();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private static String formatToSignlePhone(String phone) {
        String preformated = phone.replaceAll("\\s+", "");
        if (preformated.substring(0) == "+" && preformated.substring(1) == "7") {
            preformated = preformated.replaceAll("\\+7", "");
        } else if ((preformated.substring(0) == "8" || preformated.substring(0) == "7")
            && preformated.length() == 11) {
            preformated = new StringBuilder(preformated).deleteCharAt(0).toString();
        }

        preformated = preformated.replaceAll("\\(", "").replaceAll("\\)", "").replaceAll("\\+", "")
            .replaceAll("-", "").replaceAll(" ", "");
        return preformated;
    }

    private void testRequest() {
        System.out.println("Polifer: ");
        RequestQueue queue = Volley.newRequestQueue(this);

        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
            null, null, null, null);
        if ((cur != null ? cur.getCount() : 0) > 0) {
            while (cur != null && cur.moveToNext()) {
                String id = cur.getString(
                    cur.getColumnIndex(ContactsContract.Contacts._ID));
                String name = cur.getString(cur.getColumnIndex(
                    ContactsContract.Contacts.DISPLAY_NAME));

                if (cur.getInt(cur.getColumnIndex(
                    ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {
                    Cursor pCur = cr.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                        new String[] {id}, null);
                    while (pCur.moveToNext()) {
                        String phoneNo = pCur.getString(pCur.getColumnIndex(
                            ContactsContract.CommonDataKinds.Phone.NUMBER));
                        System.out.println("Name: " + name);
                        localPhones.add(formatToSignlePhone(phoneNo));
                    }

                    pCur.close();
                }
            }
        }
        if (cur != null) {
            cur.close();
        }

        final String url = "http://85.143.214.81:27018/users";

        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
            response -> {
                // display response
                String jsonString = response.toString();
                System.out.println("Polier: " + jsonString);

                ArrayList<User> accessedUsers = new ArrayList<>(); // clear the array

                try {
                    JSONArray pages = new JSONArray(jsonString);
                    System.out.println("Julier: " + pages.length());
                    MyStorage.allUsers = new ArrayList<>(); // clear the array
                    System.out.println("Filya " + MyStorage.allUsers.size());

                    for (int i = 0; i < pages.length(); ++i) {

                        JSONObject userObject = pages.getJSONObject(i);
                        String name = userObject.getString("name");
                        String id = userObject.getString("_id");
                        String passNumber = userObject.getString("passSerial") + " " + userObject
                            .getString("passNumber");
                        if (passNumber.equals(" ")) {
                            passNumber = "";
                        }
                        String password = userObject.getString("password");
                        String surname = userObject.getString("surname");
                        String secondName = userObject.getString("secondName");
                        String phoneNumber = userObject.getString("phoneNumber");

                        User user = new User(id, password, name, surname, secondName, phoneNumber);

                        SharedPreferences sharedPref =
                            getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                        String myPassNumber = sharedPref.getString("passNumber", "nill");

                        ArrayList<String> subAccessedPhhones = new ArrayList<>();

                        for (int j = 0; j < accesedPhones.length; ++j) {
                            subAccessedPhhones.add(accesedPhones[j]);
                            System.out.println("JOJO" + accesedPhones[j]);
                        }

                        System.out.println("KOSYA: " + myPassNumber + " " + phoneNumber);

                        if (phoneNumber.equals(myPassNumber)) {

                            MyStorage.currentUser = user;
                            MyStorage.currentUser.placePasport =
                                userObject.getString("placePasport");

                            System.out.println("My user id: " + MyStorage.currentUser.id);
                            MyStorage.currentUser.facePasport = userObject.getString("facePasport");
                            MyStorage.currentUser.passNumber = passNumber;
                            MyStorage.currentUser.timePasport = userObject.getString("timePasport");
                            MyStorage.currentUser.birthday = userObject.getString("birthday");
                            MyStorage.currentUser.INN = userObject.getString("INN");
                            MyStorage.currentUser.expiriedDateUnix =
                                userObject.getInt("experiedDate");
                            MyStorage.currentUser.registration =
                                userObject.getString("registration");
                            MyStorage.currentUser.surname = surname;
                            MyStorage.currentUser.bill = userObject.getString("bill");
                            MyStorage.currentUser.BIC = userObject.getString("BIC");
                            MyStorage.currentUser.bankName = userObject.getString("bankName");
                            MyStorage.currentUser.bankBill = userObject.getString("bankBill");
                            MyStorage.currentUser.bankCard = userObject.getString("bankCard");
                            MyStorage.currentUser.fiasCity = userObject.getString("fiasCity");
                            MyStorage.currentUser.fiasIndex = userObject.getString("fiasIndex");
                            MyStorage.currentUser.fiasOblast = userObject.getString("fiasOblast");
                            MyStorage.currentUser.fiasStreet = userObject.getString("fiasStreet");
                            MyStorage.currentUser.fiasStreetNumber =
                                userObject.getString("fiasStreetNumber");
                            MyStorage.currentUser.fiasApartament =
                                userObject.getString("fiasApartament");
                            MyStorage.currentUser.fiasCityId = userObject.getString("fiasCityId");
                            MyStorage.currentUser.fiasStreetId =
                                userObject.getString("fiasStreetId");
                            MyStorage.currentUser.codeSubdivision = userObject.getString("codeSubdivision");
                            MyStorage.currentUser.fiasOblastId =
                                userObject.getString("fiasOblastId");

                            String token = FirebaseInstanceId.getInstance().getToken();
                            if (token != null && !token.isEmpty()) {
                                MyStorage.currentUser.token = token;
                                MyStorage.refreshOnServer();
                            }
                        } else {

                            MyStorage.allUsers.add(user);
                            if (localPhones.contains(formatToSignlePhone(user.phoneNumber))
                                || subAccessedPhhones
                                .contains(formatToSignlePhone(user.phoneNumber))) {
                                accessedUsers.add(user);
                                System.out.println(userObject);
                                System.out.println(userObject.has("token"));
                                if (userObject.has("token")) {
                                    user.token = userObject.getString("token");
                                }
                            }
                        }
                    }

                    final ListView listView = (ListView) findViewById(R.id.listView);
                    // создаем адаптер'
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                        @Override
                        public void onItemClick(AdapterView<?> parent, View view,
                            int position, long id) {

                            MyStorage.oponentId = accessedUsers.get(position).id;
                            MyStorage.oponentUser = accessedUsers.get(position);
                            startActivity(new Intent(UsersActivity.this, ChatActivity.class));
                        }
                    });

                    ArrayList<String> userNames = new ArrayList<String>();

                    for (int i = 0; i < accessedUsers.size(); i++) {
                        String name = accessedUsers.get(i).name;
                        userNames.add(name + ", " + accessedUsers.get(i).surname);
                    }

                    ArrayList<String> ids = new ArrayList<String>();

                    for (int i = 0; i < accessedUsers.size(); i++) {
                        ids.add(accessedUsers.get(i).id);
                    }

                    ArrayList<String> subtexts = new ArrayList<String>();

                    for (int i = 0; i < accessedUsers.size(); i++) {
                        subtexts.add(accessedUsers.get(i).phoneNumber);
                    }

                    UserList adapter = new UserList(UsersActivity.this, userNames, ids, subtexts);

                    // присваиваем адаптер списку
                    listView.setAdapter(adapter);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                if (!UsersActivity.this.isDestroyed() && mDialog != null && mDialog.isShowing()) {
                                    mDialog.dismiss();
                                }
                            } catch (Exception e) {
                                Crashlytics.logException(e);
                            }
                        }
                    });
                } catch (JSONException e) {
                    System.out.println("FOCUSDosk");
                    e.printStackTrace();
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error.Response", error.toString());
                }
            }
        );

        getRequest.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 50000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 50000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        queue.add(getRequest);
    }

    @Override
    protected void onDestroy() {
        if (mDialog != null && mDialog.isShowing()) {
            mDialog = null;
        }
        super.onDestroy();
    }

    public static String getJSONStringFromURL(String urlString) throws IOException, JSONException {
        HttpURLConnection urlConnection = null;
        URL url = new URL(urlString);
        urlConnection = (HttpURLConnection) url.openConnection();
        urlConnection.setRequestMethod("GET");
        urlConnection.setReadTimeout(10000 /* milliseconds */);
        urlConnection.setConnectTimeout(15000 /* milliseconds */);
        urlConnection.setDoOutput(true);
        urlConnection.connect();

        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder sb = new StringBuilder();

        String line;
        while ((line = br.readLine()) != null) {
            sb.append(line + "\n");
        }
        br.close();

        String jsonString = sb.toString();
        System.out.println("JSON: " + jsonString);

        return jsonString;
    }

    private HashMap<String, String> convert(String str) {
        String[] tokens = str.split("&");
        HashMap<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < tokens.length; i++) {
            String[] strings = tokens[i].split("=");
            if (strings.length == 2) {
                map.put(strings[0], strings[1].replaceAll("%2C", ","));
            }
        }

        return map;
    }
}