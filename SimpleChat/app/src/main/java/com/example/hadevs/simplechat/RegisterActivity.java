package com.example.hadevs.simplechat;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.Editable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class RegisterActivity extends AppCompatActivity {


    public static final String PREFS_NAME = "SimpleChat";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        setButtonsListeners();
        setTitle("Регистрация");
        final EditText passNumberField = (EditText) findViewById(R.id.editText2);
        passNumberField.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length()-passNumberField.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                String phone = string.replaceAll("[^\\d]", "");

                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {
                    if (phone.length() >= 4 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = phone.substring(0, 4) + " " + phone.substring(4);
                        passNumberField.setText(ans);
                        passNumberField.setSelection(passNumberField.getText().length()-cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });

        final EditText phoneNumberField = (EditText) findViewById(R.id.editText5);
        phoneNumberField.addTextChangedListener(new PhoneNumberFormattingTextWatcher() {
            //we need to know if the user is erasing or inputing some new character
            private boolean backspacingFlag = false;
            //we need to block the :afterTextChanges method to be called again after we just replaced the EditText text
            private boolean editedFlag = false;
            //we need to mark the cursor position and restore it after the edition
            private int cursorComplement;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //we store the cursor local relative to the end of the string in the EditText before the edition
                cursorComplement = s.length()-phoneNumberField.getSelectionStart();
                //we check if the user ir inputing or erasing a character
                if (count > after) {
                    backspacingFlag = true;
                } else {
                    backspacingFlag = false;
                }
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // nothing to do here =D
            }

            @Override
            public void afterTextChanged(Editable s) {
                String string = s.toString();
//                if (string.length() == 11) {
//                    string = new StringBuilder(string).deleteCharAt(0).toString();
//                }
                //what matters are the phone digits beneath the mask, so we always work with a raw string with only digits
                System.out.println("JOSTER " + string.replaceAll("[^\\d]", ""));
                String phone = string.replaceAll("[^\\d]", "").substring(1);



                //if the text was just edited, :afterTextChanged is called another time... so we need to verify the flag of edition
                //if the flag is false, this is a original user-typed entry. so we go on and do some magic
                if (!editedFlag) {

                    //we start verifying the worst case, many characters mask need to be added
                    //example: 999999999 <- 6+ digits already typed
                    // masked: (999) 999-999
                    if (phone.length() >= 8 && !backspacingFlag){
                        editedFlag = true;
                        String ans = "+7 (" + phone.substring(0, 3) + ") " + phone.substring(3,6) + "-" + phone.substring(6,8) + "-" + phone.substring(8);
                        phoneNumberField.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        phoneNumberField.setSelection(phoneNumberField.getText().length()-cursorComplement);


                    } else if (phone.length() >= 6 && !backspacingFlag) {
                        //we will edit. next call on this textWatcher will be ignored
                        editedFlag = true;
                        //here is the core. we substring the raw digits and add the mask as convenient
                        String ans = "+7 (" + phone.substring(0, 3) + ") " + phone.substring(3,6) + "-" + phone.substring(6);
                        phoneNumberField.setText(ans);
                        //we deliver the cursor to its original position relative to the end of the string
                        phoneNumberField.setSelection(phoneNumberField.getText().length()-cursorComplement);

                        //we end at the most simple case, when just one character mask is needed
                        //example: 99999 <- 3+ digits already typed
                        // masked: (999) 99
                    } else if (phone.length() >= 3 && !backspacingFlag) {
                        editedFlag = true;
                        String ans = "+7 (" +phone.substring(0, 3) + ") " + phone.substring(3);
                        phoneNumberField.setText(ans);
                        phoneNumberField.setSelection(phoneNumberField.getText().length()-cursorComplement);
                    }
                    // We just edited the field, ignoring this cicle of the watcher and getting ready for the next
                } else {
                    editedFlag = false;
                }
            }
        });

    }

    private View.OnClickListener loginButtonClicked = new View.OnClickListener() {
        public void onClick(View v) {

            EditText nameField = (EditText) findViewById(R.id.editText3);
            EditText secondNameField = (EditText) findViewById(R.id.editText);
            EditText paronymicField = (EditText) findViewById(R.id.editText4);
            EditText passNumberField = (EditText) findViewById(R.id.editText2);
            EditText phoneNumberField = (EditText) findViewById(R.id.editText5);

            String name = nameField.getText().toString();
            String secondName = secondNameField.getText().toString();
            String paronymic = paronymicField.getText().toString();
            final String passnumber = passNumberField.getText().toString();
            String phoneNumber = phoneNumberField.getText().toString();

            if (TextUtils.isEmpty(name) || TextUtils.isEmpty(secondName) || TextUtils.isEmpty(paronymic) || TextUtils.isEmpty(passnumber) || TextUtils.isEmpty(phoneNumber)) {
                AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                builder.setTitle("Ошибка")
                        .setMessage("Необходимо заполнить все поля.")
                        .setCancelable(true)
                        .setNegativeButton("ОК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            } else {
                // all fields is filled. we can continue logging

                HashMap<String, String> data = new HashMap<String, String>();
                data.put("name", name.replaceAll("\\s+",""));
                data.put("surname", secondName.replaceAll("\\s+",""));
                data.put("secondName", paronymic.replaceAll("\\s+",""));
                data.put("password", passnumber);
                data.put("phoneNumber", phoneNumber);
                data.put("invitedBy", MyStorage.currentUser.id);

                AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);
                asyncHttpPost.setListener(new AsyncHttpPost.Listener(){
                    @Override
                    public void onResult(String result) {
                        // do something, using return value from network
                        AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                        System.out.println("COJULER");
                        System.out.println(result);
                        String message = "Регистрация успешна!";
                        if (result.equals("duplicate")) {
                            message = "Ошибка";
                            builder.setTitle("Неверные данные или такой пользовать уже существует.")
                                    .setMessage(message)
                                    .setCancelable(true)
                                    .setNegativeButton("ОК",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        } else {
                            builder.setTitle("Готово")
                                    .setMessage(message)
                                    .setCancelable(true)
                                    .setNegativeButton("ОК",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    }
                });
                asyncHttpPost.execute("http://85.143.214.81:27018/register");

            }
        }
    };

    private void setButtonsListeners() {
        Button enterButton = (Button)findViewById(R.id.Войти);
        enterButton.setOnClickListener(loginButtonClicked);
    }
}
