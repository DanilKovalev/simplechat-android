package com.example.hadevs.simplechat;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;

public class FIASSelectActivity extends AppCompatActivity {

    FIASObject fiasObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fiasselect);
        configureFiasObject();

        final EditText editText = findViewById(R.id.editText14);
        editText.setOnClickListener(view -> {
            if (fiasObject != null) {
                editText.setText("");
            }
        });

        String type = getIntent().getStringExtra("type"); if (type == null) type = "";
        System.out.println("JOSEF JOSTER: " + type);

        String prevObjectId = getIntent().getStringExtra("prevObjectId"); if (prevObjectId == null) prevObjectId = "";

        Observable<String> obs3 = RxTextView.textChanges(editText).filter(charSequence -> charSequence.length() > 0).debounce(300, TimeUnit.MILLISECONDS).map(charSequence ->  charSequence.toString());
        String finalPrevObjectId = prevObjectId;
        String finalType = type;
        String finalType3 = type;
        obs3.doOnError(throwable -> Log.e("F", "Throwable " + throwable.getMessage()))
                .subscribe(string -> {
                    String finalType2 = finalType3;
                    FIASManager.search(finalType, finalPrevObjectId, string, FIASSelectActivity.this, (fiasObjects, additionalInfo) -> {
                        if (fiasObjects.isEmpty()) {
                            editText.setTextColor(Color.RED);
                        } else {
                            editText.setTextColor(Color.BLACK);
                        }
                            ArrayList<String> fiasObjectNames = new ArrayList<>();
                            for (int i = 0; i < fiasObjects.size(); i ++) {
                                fiasObjectNames.add(fiasObjects.get(i).name);
                            }

                            ListView listView = findViewById(R.id.fiasListView);
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                                    android.R.layout.simple_list_item_1, fiasObjectNames);
                            listView.setAdapter(adapter);

                            String finalType1 = finalType2;
                            listView.setOnItemClickListener((adapterView, view, i, l) -> {
                                FIASObject selectedFiasObject = fiasObjects.get(i);
                                Intent intent = new Intent();
                                Bundle extra = new Bundle();
                                extra.putString("fiasId", selectedFiasObject.id);
                                extra.putString("fiasName", selectedFiasObject.name);
                                extra.putString("additionalInfo", additionalInfo);

                                intent.putExtras(extra);

                                int resultCode = 1;
                                if (finalType1.equals("oblast")) {
                                    resultCode = 1;
                                } else if (finalType1.equals("city")) {
                                    resultCode = 2;
                                } else if (finalType1.equals("streetName")) {
                                    resultCode = 3;
                                } else if (finalType1.equals("streetNumber")) {
                                    resultCode = 4;
                                }
                                setResult(resultCode, intent);
                                finish();

                            });

                        });


                },throwable -> Log.e("f", "Throwable " + throwable.getMessage()));
    }

    void configureFiasObject() {
        if (getIntent().getStringExtra("fiasId") != null && !getIntent().getStringExtra("fiasId").isEmpty() &&
                getIntent().getStringExtra("fiasName") != null && !getIntent().getStringExtra("fiasName").isEmpty()) {
            fiasObject = new FIASObject(getIntent().getStringExtra("fiasName"), getIntent().getStringExtra("fiasId"));
        }
    }

}

interface FIASSelectCallbackInterface {
    void onTaskFinished(FIASObject fiasObject, String additionalInfo);
}

