package com.example.hadevs.simplechat;

/**
 * Created by Hadevs on 25.10.2017.
 */


import android.*;
import android.Manifest;
import android.app.TabActivity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.TypedValue;
import android.widget.TabHost;
import android.widget.TextView;

import java.util.Observable;
import java.util.Observer;

public class TabBarActivity extends TabActivity {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TabHost tabHost = getTabHost();
        TabHost.TabSpec spec;
        Intent intent;

        intent = new Intent().setClass(this, UsersActivity.class);
        spec = tabHost.newTabSpec("First").setIndicator("Пользователи")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, SettingsActivity.class);
        spec = tabHost.newTabSpec("Second").setIndicator("Настройки")
                .setContent(intent);
        tabHost.addTab(spec);

        intent = new Intent().setClass(this, ChatsActivity.class);
        spec = tabHost.newTabSpec("Third").setIndicator("Чаты")
                .setContent(intent);
        tabHost.addTab(spec);


        tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor("#3F51B5"));
        tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor("#3F51B5"));
        tabHost.getTabWidget().getChildAt(2).setBackgroundColor(Color.parseColor("#3F51B5"));
        TextView firstTextView = (TextView) tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
        firstTextView.setTextColor(Color.WHITE);
        firstTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP,13);

        TextView secondTextView = (TextView) tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
        secondTextView.setTextColor(Color.WHITE);
        TextView thirdTextView = (TextView) tabHost.getTabWidget().getChildAt(2).findViewById(android.R.id.title);
        thirdTextView.setTextColor(Color.WHITE);

//        ChatActivity

        if (TabBarActivity.this.checkSelfPermission(android.Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_CONTACTS}, 77);
        }

        if (TabBarActivity.this.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 78);
        }

        if (TabBarActivity.this.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 79);
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 77) {

            NotificationCenter.defaultCenter().postNotification("permissionsUpdated");
//            Intent i = getBaseContext().getPackageManager().getLaunchIntentForPackage( getBaseContext().getPackageName() );
//            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(i);
        }
    }
}