package com.example.hadevs.simplechat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class ChatsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chats);

        setTitle("Чаты");

    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStart() {
        super.onStart();
        final ArrayList<FirMessage> firMessages = new ArrayList<FirMessage>();

        FirebaseDatabase storage = FirebaseDatabase.getInstance();
        DatabaseReference reference = storage.getReference();

        System.out.println("FERMER: " + MyStorage.currentUser.id);

        DatabaseReference chatsReference = reference
                .child("users")
                .child(MyStorage.currentUser.id)
                .child("chats");

        ValueEventListener postListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayList<String> userIds = new ArrayList<>();
                ArrayList<FirMessage> firMessages = new ArrayList<>();
                for (int i = 0; i < MyStorage.allUsers.size(); i++) {
                    userIds.add(MyStorage.allUsers.get(i).id);
                }

                for (DataSnapshot chat: dataSnapshot.getChildren()) {
                    DataSnapshot lastMessage = chat;
                    for (DataSnapshot message: chat.getChildren()) {
                        // here is a message
                        lastMessage = message;
                    }

                    System.out.println("mamich");
                    System.out.println(lastMessage.child("date").getValue());
                    System.out.println(lastMessage.child("lastName").getValue());
                    System.out.println(lastMessage.child("status").getValue());
                    System.out.println(lastMessage.child("videoID").getValue());
                    System.out.println(lastMessage.child("photoID").getValue());
                    System.out.println(lastMessage.child("type").getValue());
                    System.out.println(lastMessage.child("text").getValue());
                    System.out.println(lastMessage.child("surname").getValue());
                    System.out.println(lastMessage.child("sender").getValue());
                    System.out.println(lastMessage.child("name").getValue());


                    FirMessage firMessage = new FirMessage(lastMessage.child("date").getValue().toString(), lastMessage.child("lastName").getValue().toString(), lastMessage.child("name").getValue().toString(), lastMessage.child("sender").getValue().toString(), lastMessage.child("surname").getValue().toString(), lastMessage.child("text").getValue().toString(), lastMessage.child("type").getValue().toString(), lastMessage.child("photoID").getValue().toString(), lastMessage.child("videoID").getValue().toString(), lastMessage.child("status").getValue().toString(), chat.getKey());

                    if (userIds.contains(firMessage.receiver)) {
                        firMessages.add(firMessage);
                    }
                }

                //sort firMessages

                Collections.sort(firMessages, new Comparator<FirMessage>() {
                    @Override
                    public int compare(FirMessage firMessage, FirMessage t1) {
                        if (Double.parseDouble(firMessage.date) > Double.parseDouble(t1.date)) {
                            return -1;
                        } else {
                            return 1;
                        }
                    }
                });

                ListView lvMain = (ListView) findViewById(R.id.listView);

                final ArrayList<User> users = new ArrayList<User>();

                for (int i = 0; i < firMessages.size(); i++) {
                    for (int j = 0; j < MyStorage.allUsers.size(); j++) {
                        User user = MyStorage.allUsers.get(j);
                        FirMessage message = firMessages.get(i);
                        if (user.id.equals(message.receiver)) {
                            users.add(user);
                        }
                    }
                }

                lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view,
                                            int position, long id) {
                        User user = users.get(position);
                        MyStorage.oponentId = user.id;
                        MyStorage.oponentUser = user;
                        startActivity(new Intent(ChatsActivity.this, ChatActivity.class));

                    }
                });

                System.out.println("ETO CHTO Da? + " + firMessages.size() + " " + users.size());
                ChatAdapter adapter = new ChatAdapter(ChatsActivity.this, firMessages, users);
                lvMain.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                System.out.println("loadPost:onCancelled " + databaseError.toException());
            }
        };


        chatsReference.addValueEventListener(postListener);
    }
}
