package com.example.hadevs.simplechat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;

public class PersonalAccount extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_account);
        setTitle("Данные о лицевом счете");
        addButtonListener();
        fillData();
    }

    private void fillData() {
        final EditText billField = findViewById(R.id.editText6);
        billField.setText(MyStorage.currentUser.bill.replace("null", ""));

        final EditText BICField = findViewById(R.id.editText7);
        BICField.setText(MyStorage.currentUser.BIC.replace("null", ""));

        final EditText bankNameField = findViewById(R.id.editText8);
        bankNameField.setText(MyStorage.currentUser.bankName.replace("null", ""));

        final EditText bankBillField = findViewById(R.id.editText9);
        bankBillField.setText(MyStorage.currentUser.bankBill.replace("null", ""));
        final EditText fioField = findViewById(R.id.editText11);

        fioField.setText(MyStorage.currentUser.surname + " " + MyStorage.currentUser.name + " " + MyStorage.currentUser.secondName);

        final EditText bankCardField = findViewById(R.id.editText12);
        bankCardField.setText(MyStorage.currentUser.bankCard.replace("null", ""));

        System.out.println("RETEN: " + MyStorage.currentUser.bankCard);
    }

    private void addButtonListener() {
        final Button button = (Button) findViewById(R.id.button3);
        button.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText billField = findViewById(R.id.editText6);
                final String bill = billField.getText().toString();

                final EditText BICField = findViewById(R.id.editText7);
                final String BIC = BICField.getText().toString();

                final EditText bankNameField = findViewById(R.id.editText8);
                final String bankName = bankNameField.getText().toString();

                final EditText bankBillField = findViewById(R.id.editText9);
                final String bankBill = bankBillField.getText().toString();
                final EditText fioField = findViewById(R.id.editText11);

                final String fio = fioField.getText().toString();

                final EditText bankCardField = findViewById(R.id.editText12);
                final String bankCard = bankCardField.getText().toString();

                if (bill.isEmpty() || BIC.isEmpty() || bankName.isEmpty() || bankBill.isEmpty() || fio.isEmpty() || bankCard.isEmpty()) {
                    Toast.makeText(PersonalAccount.this, "Заполните все поля.", Toast.LENGTH_SHORT).show();
                } else {
                    MyStorage.currentUser.bill = bill;
                    MyStorage.currentUser.BIC = BIC;
                    MyStorage.currentUser.bankName = bankName;
                    MyStorage.currentUser.bankBill = bankBill;
                    MyStorage.currentUser.bankCard = bankCard;

                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("name", MyStorage. currentUser.name.replaceAll("\\s+",""));
                    data.put("_id", MyStorage.currentUser.id);
                    data.put("surname", MyStorage.currentUser.surname.replaceAll("\\s+",""));
                    data.put("secondName", MyStorage.currentUser.secondName.replaceAll("\\s+",""));
                    data.put("passNumber", MyStorage.currentUser.passNumber);
                    data.put("placePasport", MyStorage.currentUser.placePasport);
                    data.put("facePasport", MyStorage.currentUser.facePasport);
                    data.put("timePasport", MyStorage.currentUser.timePasport);
                    data.put("birthday", MyStorage.currentUser.birthday);
                    data.put("INN", MyStorage.currentUser.INN);
                    data.put("registration", MyStorage.currentUser.registration);
                    data.put("token", MyStorage.currentUser.token);
                    data.put("phoneNumber", MyStorage.currentUser.phoneNumber);
                    data.put("bill", MyStorage.currentUser.bill);
                    data.put("BIC", MyStorage.currentUser.BIC);
                    data.put("bankName", MyStorage.currentUser.bankName);
                    data.put("bankBill", MyStorage.currentUser.bankBill);
                    data.put("bankCard", MyStorage.currentUser.bankCard);

                    System.out.println("Out coming hashmap: " + data);

                    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);

                    asyncHttpPost.setListener(new AsyncHttpPost.Listener(){
                        @Override
                        public void onResult(String result) {
                            // do something, using return value from network

                        }
                    });

                    asyncHttpPost.execute("http://85.143.214.81:27018/editUser");

                    Toast.makeText(PersonalAccount.this, "Данные сохранены", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}
