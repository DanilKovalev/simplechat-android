package com.example.hadevs.simplechat;

import android.app.Activity;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import co.intentservice.chatui.views.*;

/**
 * Created by Hadevs on 03.12.2017.
 */

    public class ChatAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private final ArrayList<FirMessage> messages;
    private final ArrayList<User> users;


    public ChatAdapter(Activity context,
                       ArrayList<FirMessage> messages, ArrayList<User> users) {
        super(context, R.layout.list_chat, new String[messages.size()]);
        this.context = context;
        this.messages = messages;
        this.users = users;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        final View rowView = inflater.inflate(R.layout.list_chat, null, true);
        TextView txtTitle = (TextView) rowView.findViewById(R.id.txt);
        TextView subtextTitle = (TextView) rowView.findViewById(R.id.txt2);
        TextView timeLabel = (TextView) rowView.findViewById(R.id.txt3);
        TextView readLabel = (TextView) rowView.findViewById(R.id.txt4);
        final ImageView imageView = (ImageView) rowView.findViewById(R.id.img);

        FirMessage message = messages.get(position);
        User user = users.get(position);

        txtTitle.setText(user.name + " " + user.surname);
        if (message.type.equals("photo")) {
            subtextTitle.setText("[Изображение]");
        } else if (message.type.equals("video")) {
            subtextTitle.setText("[Видеозапись]");
        } else {
            subtextTitle.setText(message.text);
        }

        System.out.println("Rockin: " + message.text);

        if (message.status.equals("read")) {
            //read
            readLabel.setText("Прочитано");
            readLabel.setTextColor(Color.GRAY);

        } else {
            // unread
            if (!message.sender.equals(MyStorage.currentUser.id)) {
                readLabel.setText("Новое");
                readLabel.setTextColor(Color.BLUE);

            } else {
                readLabel.setText("Доставлено");
                readLabel.setTextColor(Color.BLUE);

            }
        }

        // load url for current user
        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imagesRef = storageRef.child("avatars");

        imagesRef.child(user.id).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide
                        .with(getContext())
                        .load(uri) // the uri you got from Firebase
                        .apply(RequestOptions.circleCropTransform())
                        .into(imageView); //Your imageView variable
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                Glide
                        .with(getContext())
                        .load(R.drawable.ic_launcher_round) // the uri you got from Firebase
                        .apply(RequestOptions.circleCropTransform())
                        .into(imageView); //Your imageView variable
            }
        });



        String result = message.date.substring(0, message.date.indexOf("."));
        long timestamp = Long.valueOf(result);

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;
        long unixSeconds = timestamp;
        Date date = new Date(unixSeconds*1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM hh:mm"); // the format of your date
        long timeDifference = System.currentTimeMillis() - timestamp;

        if (timeDifference < oneDayInMillis) {
            sdf = new SimpleDateFormat("hh:mm") ;
        }

        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        sdf.setTimeZone(tz); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);

        timeLabel.setText(formattedDate);


        return rowView;
    }

}
