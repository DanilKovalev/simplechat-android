package com.example.hadevs.simplechat;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Toast;

import com.appunite.appunitevideoplayer.PlayerActivity;
import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.fab.FloatingActionsMenu;
import co.intentservice.chatui.models.ChatMessage;
import co.intentservice.chatui.views.ItemRecvView;
import co.intentservice.chatui.views.ItemSentView;
import co.intentservice.chatui.views.MessageView;

import static android.R.attr.bitmap;
import static android.provider.AlarmClock.EXTRA_MESSAGE;
import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;

public class ChatActivity extends AppCompatActivity {

    static boolean active = false;
    final String oponentId = MyStorage.oponentId;
    final String myId = MyStorage.currentUser.id;
    ArrayList<FirMessage> firMessages = new ArrayList<FirMessage>();
    String myAvatarUrl = "http://www.sheffield.com/wp-content/uploads/2013/06/placeholder.png";
    String enemyAvatarUrl = "http://www.sheffield.com/wp-content/uploads/2013/06/placeholder.png";
    int functionsCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        setTitle(MyStorage.oponentUser.name + " " + MyStorage.oponentUser.surname);

        StorageReference storageRef = FirebaseStorage.getInstance().getReference();
        StorageReference imagesRef = storageRef.child("avatars");

        System.out.println("USER ID " + MyStorage.oponentUser.surname + " " + MyStorage.oponentUser.token);

        // load url for current user
       imagesRef.child(MyStorage.currentUser.id).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                myAvatarUrl = uri.toString();
                functionsCounter += 1;
                launchChatView();
            }
        }).addOnFailureListener(new OnFailureListener() {
           @Override
           public void onFailure(@NonNull Exception exception) {
               // Handle any errors
               functionsCounter += 1;
               launchChatView();
           }
       });

        imagesRef.child(MyStorage.oponentId).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                // Got the download URL for 'users/me/profile.png'
                System.out.println("Rockstar" + uri);

                enemyAvatarUrl = uri.toString();
                functionsCounter += 1;
                launchChatView();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle any errors
                functionsCounter += 1;
                launchChatView();
            }
        });


        System.out.println("Positu " + myAvatarUrl);
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setBackgroundResource(R.drawable.wa1);


    }

    private void readLastMessage() {
        System.out.println("Kisya NNAYYAYAY");
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference ref = database.getReference();
        if (!firMessages.get(firMessages.size() - 1).sender.equals(MyStorage.currentUser.id)) {
            ref.child("users").child(MyStorage.currentUser.id).child("chats").child(MyStorage.oponentId).child(Integer.toString(firMessages.size() - 1)).child("status").setValue("read");
            ref.child("users").child(MyStorage.oponentId).child("chats").child(MyStorage.currentUser.id).child(Integer.toString(firMessages.size() - 1)).child("status").setValue("read");
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }

    private void launchChatView() {
    if (functionsCounter == 2) {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        ChatView chatView = (ChatView) findViewById(R.id.chat_view);
        chatView.setAvatar(myAvatarUrl);



        chatView.setOnAvatarClickListener(new ChatView.OnAvatarClickListener() {
            @Override
            public void clickOnAvatar(String url) {
                System.out.println("AVATOS: " + url);
                Intent intent = new Intent(ChatActivity.this, FullImage.class);
                intent.putExtra("url", url);
                startActivity(intent);
            }
        });

        chatView.setOnPhotoMessageListener(new ChatView.OnPhotoMessageListener() {
            @Override
            public boolean sendPhoto() {
                System.out.println("POWER");

                AlertDialog.Builder sourceBuilder = new AlertDialog.Builder(ChatActivity.this);
                sourceBuilder.setTitle("Приложение")
                        .setMessage("Выберите метод отправки файла.")
                        .setCancelable(true)

                        .setNeutralButton("Отменить",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setPositiveButton("Фотография",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                                        builder.setTitle("Приложение")
                                                .setMessage("Выберите метод отправки фотографии.")
                                                .setCancelable(true)

                                                .setNeutralButton("Отменить",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                            }
                                                        })
                                                .setPositiveButton("Фотоальбом",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                                                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                                startActivityForResult(pickPhoto , 1);//one can be replaced with any action code
                                                                dialog.cancel();
                                                            }
                                                        })
                                                .setNegativeButton("Камера",
                                                        new DialogInterface.OnClickListener() {
                                                            public void onClick(DialogInterface dialog, int id) {
                                                                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                                startActivityForResult(takePicture, 0);//zero can be replaced with any action code
                                                                dialog.cancel();
                                                            }
                                                        });

                                        AlertDialog alert = builder.create();
                                        alert.show();
                                        dialog.cancel();
                                    }
                                })
                        .setNegativeButton("Видео",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent pickIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                        pickIntent.setType("video/*");
                                        startActivityForResult(pickIntent, 99);
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alert = sourceBuilder.create();
                alert.show();

                return true;
            }
        });

        chatView.chatListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position,
                                    long id) {

                final FirMessage firMessage = firMessages.get(position);

                // this is media item

                if (firMessage.type.equals("photo")) {
                    // this is photo item
                    Intent intent = new Intent(ChatActivity.this, FullImage.class);
                    intent.putExtra("url", firMessage.photoID);
                    startActivity(intent);
                } else if (firMessage.type.equals("video")) {
                    // this is video item
                    AlertDialog.Builder builder = new AlertDialog.Builder(ChatActivity.this);
                    builder.setTitle("Видео")
                            .setMessage("Что вы хотите сделать с видео?")
                            .setCancelable(true)

                            .setNeutralButton("Отменить",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .setPositiveButton("Проиграть",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            String url = firMessage.videoID;

                                            startActivity(PlayerActivity.getVideoPlayerIntent(ChatActivity.this,
                                                    url,
                                                    "Видеозапись"));

                                            dialog.cancel();
                                        }
                                    })
                            .setNegativeButton("Поделиться",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            Intent i = new Intent(Intent.ACTION_SEND);
                                            i.setType("text/plain");
                                            i.putExtra(Intent.EXTRA_SUBJECT, "Отправка видеозаписи");
                                            i.putExtra(Intent.EXTRA_TEXT, firMessage.videoID);
                                            startActivity(Intent.createChooser(i, "Видео"));

                                            dialog.cancel();
                                        }
                                    });

                    AlertDialog alert = builder.create();
                    alert.show();

                }
            }
        });

        chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener(){
            @Override
            public boolean sendMessage(ChatMessage chatMessage){
                // perform actual message sending
                FirebaseDatabase database = FirebaseDatabase.getInstance();

                //set to me
                DatabaseReference myRef = database.getReference("users").child(myId).child("chats").child(oponentId);
                String date =  Long.toString(System.currentTimeMillis() / 1000L) + ".0";
                String lastName = MyStorage.currentUser.secondName;
                String name = MyStorage.currentUser.name;
                String sender = MyStorage.currentUser.id;
                String surname = MyStorage.currentUser.surname;
                String text = chatMessage.getMessage();
                String type = "text";

                FirMessage firMessage = new FirMessage(date, lastName, name, sender, surname, text, type, "", "", "unread", oponentId);


                // set to oponement
                DatabaseReference notMyRef = database.getReference("users").child(oponentId).child("chats").child(myId);
                MessagingManager.sendMessage(MyStorage.oponentUser, MyStorage.currentUser.name + ": " + chatMessage.getMessage());

                firMessages.add(firMessage);
                myRef.setValue(firMessages);
                notMyRef.setValue(firMessages);

                System.out.println(chatMessage);

                readLastMessage();

                return true;
            }
        });


        SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        String encoded = sharedPref.getString("backgroundImage", "nill");
        if (encoded != "nill") {
            byte[] imageAsBytes = Base64.decode(encoded, Base64.DEFAULT);
            imageView.setImageBitmap(BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length));
        }

        loadMessages();
    }
}


    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch(requestCode) {
            case 0:
                if(resultCode == RESULT_OK){
                    Uri file = imageReturnedIntent.getData();
                    //handle image
                    Uri selectedImage = imageReturnedIntent.getData();
                    // CAMERA
//                    Uri file = imageReturnedIntent.getData();
                    // GALERRY
//                    imageview.setImageURI(selectedImage);
                    final String id = UUID.randomUUID().toString();
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReference();
                    StorageReference riversRef = storageRef.child("images/"+id);
                    UploadTask  uploadTask = riversRef.putFile(file);

// Register observers to listen for when the download is done or if it fails
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();

                            DatabaseReference myRef = database.getReference("users").child(myId).child("chats").child(oponentId);
                            String date =  Long.toString(System.currentTimeMillis() / 1000L) + ".0";
                            String lastName = MyStorage.currentUser.secondName;
                            String name = MyStorage.currentUser.name;
                            String sender = MyStorage.currentUser.id;
                            String surname = MyStorage.currentUser.surname;
                            String text = "";
                            String type = "photo";

                            FirMessage firMessage = new FirMessage(date, lastName, name, sender, surname, text, type, downloadUrl, "", "unread", oponentId);

                            // set to oponement
                            DatabaseReference notMyRef = database.getReference("users").child(oponentId).child("chats").child(myId);
                            MessagingManager.sendMessage(MyStorage.oponentUser, MyStorage.currentUser.name + ": [Изображение]");

                            firMessages.add(firMessage);
                            myRef.setValue(firMessages);
                            notMyRef.setValue(firMessages);
                        }
                    });
                }


                break;
            case 1:
                if(resultCode == RESULT_OK){
                    Uri file = imageReturnedIntent.getData();
                    // GALERRY
//                    imageview.setImageURI(selectedImage);
                    final String id = UUID.randomUUID().toString();
                    FirebaseStorage storage = FirebaseStorage.getInstance();
                    StorageReference storageRef = storage.getReference();
                    StorageReference riversRef = storageRef.child("images/"+id);
                    UploadTask  uploadTask = riversRef.putFile(file);

// Register observers to listen for when the download is done or if it fails
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle unsuccessful uploads
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            String downloadUrl = taskSnapshot.getDownloadUrl().toString();
                            FirebaseDatabase database = FirebaseDatabase.getInstance();

                            DatabaseReference myRef = database.getReference("users").child(myId).child("chats").child(oponentId);
                            String date =  Long.toString(System.currentTimeMillis() / 1000L) + ".0";
                            String lastName = MyStorage.currentUser.secondName;
                            String name = MyStorage.currentUser.name;
                            String sender = MyStorage.currentUser.id;
                            String surname = MyStorage.currentUser.surname;
                            String text = "";
                            String type = "photo";

                            FirMessage firMessage = new FirMessage(date, lastName, name, sender, surname, text, type, downloadUrl, "", "unread", oponentId);

                            // set to oponement
                            DatabaseReference notMyRef = database.getReference("users").child(oponentId).child("chats").child(myId);
                            MessagingManager.sendMessage(MyStorage.oponentUser, MyStorage.currentUser.name + ": [Изображение]");

                            firMessages.add(firMessage);
                            myRef.setValue(firMessages);
                            notMyRef.setValue(firMessages);
                        }
                    });
                }
                break;

            case 99:
                //handle video
                if(resultCode == RESULT_OK) {
                    // var 'file' is path, that we need
                    final ProgressDialog progressDialog = new ProgressDialog(this);
                    progressDialog.setTitle("Загрузка видео");
                    progressDialog.show();
                    Uri file = imageReturnedIntent.getData();

                    FirebaseStorage storage = FirebaseStorage.getInstance();

                    final String id = UUID.randomUUID().toString();
                    // Create a storage reference from our app
                    StorageReference storageRef = storage.getReference();
                    StorageReference riversRef = storageRef.child("video/" + id);
                    riversRef.putFile(file)
                            .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    //if the upload is successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();

                                    //and displaying a success toast
                                    Toast.makeText(getApplicationContext(), "Файл загружен", Toast.LENGTH_LONG).show();

                                    final Uri url = taskSnapshot.getDownloadUrl();
                                    final String downloadUrl = url.toString();

                                    FirebaseDatabase database = FirebaseDatabase.getInstance();

                                    DatabaseReference myRef = database.getReference("users").child(myId).child("chats").child(oponentId);
                                    String date = Long.toString(System.currentTimeMillis() / 1000L) + ".0";
                                    String lastName = MyStorage.currentUser.secondName;
                                    String name = MyStorage.currentUser.name;
                                    String sender = MyStorage.currentUser.id;
                                    String surname = MyStorage.currentUser.surname;
                                    String text = "";
                                    String type = "video";

                                    FirMessage firMessage = new FirMessage(date, lastName, name, sender, surname, text, type, "", downloadUrl, "unread", oponentId);

                                    // set to oponement
                                    DatabaseReference notMyRef = database.getReference("users").child(oponentId).child("chats").child(myId);
                                    MessagingManager.sendMessage(MyStorage.oponentUser, MyStorage.currentUser.name + ": [Видеозапись]");
                                    firMessages.add(firMessage);
                                    myRef.setValue(firMessages);
                                    notMyRef.setValue(firMessages);

                                }
                            })
                            .addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    //if the upload is not successfull
                                    //hiding the progress dialog
                                    progressDialog.dismiss();

                                    //and displaying error message
                                    Toast.makeText(getApplicationContext(), exception.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            })
                            .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                    //calculating progress percentage
                                    double progress = (100.0 * taskSnapshot.getBytesTransferred()) / taskSnapshot.getTotalByteCount();

                                    //displaying percentage in progress dialog
                                    progressDialog.setMessage("Загружено " + ((int) progress) + "%");
                                }
                            });
                }

        }
    }

    public static Bitmap drawableToBitmap (Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if(bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if(drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Glide.with(getApplicationContext()).pauseRequests();

    }

    private void loadMessages() {
        final ChatView chatView = (ChatView) findViewById(R.id.chat_view);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("users").child(myId).child("chats").child(oponentId);
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                long value=dataSnapshot.getChildrenCount();

                GenericTypeIndicator<List<FirMessage>> genericTypeIndicator =new GenericTypeIndicator<List<FirMessage>>(){};

                List<FirMessage> taskDesList=dataSnapshot.getValue(genericTypeIndicator);

                if (taskDesList != null) {

                    chatView.clearMessages();
                    firMessages = new ArrayList<FirMessage>();
                    for (int i = 0; i < taskDesList.size(); i++) {
                        String result = taskDesList.get(i).date.substring(0, taskDesList.get(i).date.indexOf("."));

                        long timestamp = Long.valueOf(result);

                        String message = taskDesList.get(i).text;
                        String avatarURL = "";

                        ChatMessage.Type type;
                        if (taskDesList.get(i).sender.equals(myId)) {
                            type = ChatMessage.Type.SENT;
                            avatarURL = myAvatarUrl;
                        } else {
                            type = ChatMessage.Type.RECEIVED;
                            avatarURL = enemyAvatarUrl;
                        }

                        String idURL = "";
                        if (taskDesList.get(i).type.equals("video")) {
                            idURL = taskDesList.get(i).videoID;
                        } else if (taskDesList.get(i).type.equals("photo")) {
                            idURL = taskDesList.get(i).photoID;
                        }

                        chatView.addMessage(new ChatMessage(message, timestamp, type, idURL, avatarURL));

                        FirMessage firMessage = new FirMessage(taskDesList.get(i).date, taskDesList.get(i).lastName, taskDesList.get(i).name, taskDesList.get(i).sender, taskDesList.get(i).surname, taskDesList.get(i).text, taskDesList.get(i).type, taskDesList.get(i).photoID, taskDesList.get(i).videoID, taskDesList.get(i).status, oponentId);
                        firMessages.add(firMessage);
                    }

                    if (active) {
                        readLastMessage();
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                System.out.println("GOFER");
            }
        });

    }
}
