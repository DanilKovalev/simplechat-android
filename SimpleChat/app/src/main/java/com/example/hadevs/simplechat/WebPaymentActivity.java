package com.example.hadevs.simplechat;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;

public class WebPaymentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offerta);
        final RequestQueue queue = Volley.newRequestQueue(WebPaymentActivity.this);

        final WebView webview = (WebView) findViewById(R.id.webveiew9123);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description,
                String failingUrl) {
                Toast.makeText(WebPaymentActivity.this, description, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);

                if (url.contains("default/draft/ok")) {
                    webview.stopLoading();

                    JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET,
                        "http://85.143.214.81:27018/activeUser/" + getIntent()
                            .getStringExtra("userId"), null,
                        response -> {
                            SharedPreferences sharedPref =
                                getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor
                                .putString("passNumber", getIntent().getStringExtra("passnumber"));
                            editor.commit();
                            runOnUiThread(() -> {
                                Intent intent =
                                    new Intent(WebPaymentActivity.this, TabBarActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                finish();
                                startActivity(intent);
                            });
                        },
                        error -> Log.d("Error.Response", error.toString())
                    );

                    queue.add(getRequest);
                } else if (url.contains("/draft/fail")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(WebPaymentActivity.this);
                    String message = "Проблема с оплатой. Попробуйте еще раз.";
                    builder.setTitle("Ошибка")
                        .setMessage(message)
                        .setCancelable(true)
                        .setNegativeButton("ОК",
                            (dialog, id) -> {
                                dialog.cancel();
                                finish();
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            }
        });

        webview.loadUrl(getIntent().getStringExtra("url"));
    }
}
