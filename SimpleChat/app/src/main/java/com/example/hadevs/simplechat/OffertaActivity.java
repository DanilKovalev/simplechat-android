package com.example.hadevs.simplechat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;

public class OffertaActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offerta);

        WebView webview = (WebView) findViewById(R.id.webveiew9123);
        webview.getSettings().setJavaScriptEnabled(true);

        webview.loadUrl("https://drive.google.com/file/d/1ziuP9WeYrGS5E-v_YmdETXgqs1wgyn4H/view?usp=sharing");

    }
}
