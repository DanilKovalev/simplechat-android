package com.example.hadevs.simplechat;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jakewharton.rxbinding.widget.RxTextView;
import com.tuyenmonkey.AutoFillEditText;

import java.io.SyncFailedException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import rx.Observable;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.L;

public class FIASActivity extends AppCompatActivity {

    FIASComp comp = new FIASComp();
    String t0 = "";
    String t1 = "";
    String t2 = "";
    String t3 = "";

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fias);

        setButtonListener();


        final AutoCompleteTextView oblastTextField = findViewById(R.id.autoCompleteTextView);
        final AutoCompleteTextView apartamentTextField = findViewById(R.id.autoCompleteTextView2);
        final AutoCompleteTextView cityTextField = findViewById(R.id.afetEmail);
        final AutoCompleteTextView streetNameTextField = findViewById(R.id.autoFillEditText);
        final AutoCompleteTextView streetNumberTextField = findViewById(R.id.autoFillEditText2);

        setListeners();

        apartamentTextField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                comp.apartament = apartamentTextField.getText().toString();
            }
        });

        if (!MyStorage.currentUser.fiasOblast.isEmpty() && !MyStorage.currentUser.fiasOblast.equals("null") && MyStorage.currentUser.fiasOblast != null) {
            comp.oblast = new FIASObject(MyStorage.currentUser.fiasOblast, MyStorage.currentUser.fiasOblastId);
            System.out.println("Masha: " + MyStorage.currentUser.fiasOblastId);
            if (MyStorage.currentUser.fiasOblast.equals("null")) {
                oblastTextField.setText("");
            } else {
                oblastTextField.setText(MyStorage.currentUser.fiasOblast);
            }
        }

        if (!MyStorage.currentUser.fiasCity.isEmpty() && !MyStorage.currentUser.fiasCity.equals("null") && MyStorage.currentUser.fiasCity != null) {
            comp.city = new FIASObject(MyStorage.currentUser.fiasCity, MyStorage.currentUser.fiasCityId);
            if (MyStorage.currentUser.fiasCity.equals("null")) {
                cityTextField.setText("");
            } else {
                cityTextField.setText(MyStorage.currentUser.fiasCity);
            }
        }

        if (!MyStorage.currentUser.fiasStreet.isEmpty() && !MyStorage.currentUser.fiasStreet.equals("null") && MyStorage.currentUser.fiasStreet != null) {
            comp.street = new FIASObject(MyStorage.currentUser.fiasStreet, MyStorage.currentUser.fiasStreetId);
            if (MyStorage.currentUser.fiasStreet.equals("null")) {
                streetNameTextField.setText("");
            } else {
                streetNameTextField.setText(MyStorage.currentUser.fiasStreet);
            }
        }

        if (!MyStorage.currentUser.fiasStreetNumber.isEmpty()) {
            if (MyStorage.currentUser.fiasStreetNumber.equals("null")) {
                streetNumberTextField.setText("");
            } else {
                streetNumberTextField.setText(MyStorage.currentUser.fiasStreetNumber);
            }

            comp.index = MyStorage.currentUser.fiasIndex;
            comp.streetNumber = new FIASObject(MyStorage.currentUser.fiasStreetNumber, "");
            setAdditionalInfoInUI();
        }

        if (MyStorage.currentUser.fiasApartament != null && !MyStorage.currentUser.fiasApartament.isEmpty() && !MyStorage.currentUser.fiasApartament.equals("null")) {
            comp.apartament = MyStorage.currentUser.fiasApartament;
            if (MyStorage.currentUser.fiasApartament.equals("null")) {
                apartamentTextField.setText("");
            } else {
                apartamentTextField.setText(MyStorage.currentUser.fiasApartament);
            }
        }
    }


    @SuppressLint("ClickableViewAccessibility")
    void setListeners() {


        final AutoCompleteTextView oblastTextField = findViewById(R.id.autoCompleteTextView);
        final AutoCompleteTextView cityTextField = findViewById(R.id.afetEmail);
        final AutoCompleteTextView streetNameTextField = findViewById(R.id.autoFillEditText);
        final AutoCompleteTextView streetNumberTextField = findViewById(R.id.autoFillEditText2);

        streetNumberTextField.setOnTouchListener((v, event) -> {
            if(MotionEvent.ACTION_UP == event.getAction())
            {
                Intent intent = new Intent(FIASActivity.this, FIASSelectActivity.class);
                if (comp.street != null && !comp.street.name.isEmpty()) {
                    if (comp.streetNumber != null && !comp.streetNumber.name.isEmpty()) {
                        intent.putExtra("fiasId", comp.streetNumber.id);
                        intent.putExtra("fiasName", comp.streetNumber.name);

                    }
                    intent.putExtra("prevObjectId", comp.street.id);
                    intent.putExtra("type", "streetNumber");
                    startActivityForResult(intent, 4);
                }
            }

            return false;
        });

        streetNameTextField.setOnTouchListener((v, event) -> {
            if(MotionEvent.ACTION_UP == event.getAction())
            {
                Intent intent = new Intent(FIASActivity.this, FIASSelectActivity.class);
                if (comp.city != null && !comp.city.name.isEmpty()) {
                    if (comp.street != null && !comp.street.name.isEmpty()) {
                        intent.putExtra("fiasId", comp.street.id);
                        intent.putExtra("fiasName", comp.street.name);
                    }
                    intent.putExtra("prevObjectId", comp.city.id);
                    intent.putExtra("type", "streetName");
                    startActivityForResult(intent, 3);
                }
            }

            return false;
        });

        cityTextField.setOnTouchListener((v, event) -> {
            if(MotionEvent.ACTION_UP == event.getAction())
            {
                Intent intent = new Intent(FIASActivity.this, FIASSelectActivity.class);
                if (comp.oblast != null && !comp.oblast.name.isEmpty()) {
                    if (comp.city != null && !comp.city.name.isEmpty()) {
                        intent.putExtra("fiasId", comp.city.id);
                        intent.putExtra("fiasName", comp.city.name);
                    }
                    intent.putExtra("prevObjectId", comp.oblast.id);
                    intent.putExtra("type", "city");
                    startActivityForResult(intent, 2);
                }
            }

            return false;
        });

        oblastTextField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(MotionEvent.ACTION_UP == event.getAction())
                {
                    Intent intent = new Intent(FIASActivity.this, FIASSelectActivity.class);
                    if (comp.oblast != null && !comp.oblast.name.isEmpty()) {
                        intent.putExtra("fiasId", comp.oblast.id);
                        intent.putExtra("fiasName", comp.oblast.name);
                    }
                    intent.putExtra("type", "oblast");
                    startActivityForResult(intent, 1);
                }

                return false;
            }
        });

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        final AutoCompleteTextView oblastTextField = findViewById(R.id.autoCompleteTextView);
        final AutoCompleteTextView cityTextField = findViewById(R.id.afetEmail);
        final AutoCompleteTextView streetNameTextField = findViewById(R.id.autoFillEditText);
        final AutoCompleteTextView streetNumberTextField = findViewById(R.id.autoFillEditText2);
        // 1 is oblast
        // 2 is city
        // 3 is street
        // 4 is streetNumber

        if (resultCode == 1) {
            Bundle extra = data.getExtras();
            String ID = extra.getString("fiasId");
            String NAME = extra.getString("fiasName");
            FIASObject selectedFIASOBJECT = new FIASObject(NAME, ID);
            oblastTextField.setText(selectedFIASOBJECT.name);
            comp.oblast = selectedFIASOBJECT;

            System.out.println("UJUJ; " + selectedFIASOBJECT);
        } else if (resultCode == 2) {
            Bundle extra = data.getExtras();
            String ID = extra.getString("fiasId");
            String NAME = extra.getString("fiasName");
            FIASObject selectedFIASOBJECT = new FIASObject(NAME, ID);
            cityTextField.setText(selectedFIASOBJECT.name);
            comp.city = selectedFIASOBJECT;
        } else if (resultCode == 3) {
            Bundle extra = data.getExtras();
            String ID = extra.getString("fiasId");
            String NAME = extra.getString("fiasName");
            FIASObject selectedFIASOBJECT = new FIASObject(NAME, ID);
            streetNameTextField.setText(selectedFIASOBJECT.name);
            comp.street = selectedFIASOBJECT;
        } else if (resultCode == 4) {
            Bundle extra = data.getExtras();
            String ID = extra.getString("fiasId");
            String NAME = extra.getString("fiasName");
            String index = extra.getString("additionalInfo"); if (index == null) index = "--";
            FIASObject selectedFIASOBJECT = new FIASObject(NAME, ID);
            comp.streetNumber = selectedFIASOBJECT;
            streetNumberTextField.setText(selectedFIASOBJECT.name);
            comp.index = index;
            setAdditionalInfoInUI();
        }
    }

    private void setButtonListener() {
        final Button saveButton = findViewById(R.id.button8);
        saveButton.setOnClickListener(view -> {

            if (comp.oblast != null && comp.streetNumber != null && comp.street != null && comp.city != null) {
                if (comp.index == null) comp.index = "";

                MyStorage.currentUser.fiasStreetNumber = comp.streetNumber.name;
                MyStorage.currentUser.fiasStreet = comp.street.name;
                MyStorage.currentUser.fiasOblast = comp.oblast.name;
                MyStorage.currentUser.fiasIndex = comp.index;
                MyStorage.currentUser.fiasCity = comp.city.name;
                MyStorage.currentUser.fiasApartament = comp.apartament;
                MyStorage.currentUser.fiasOblastId = comp.oblast.id;
                MyStorage.currentUser.fiasStreetId = comp.street.id;
                MyStorage.currentUser.fiasCityId = comp.city.id;

                MyStorage.refreshOnServer();

                AlertDialog.Builder builder = new AlertDialog.Builder(FIASActivity.this);
                builder.setTitle("Готово")
                        .setMessage("Данные заполнены.")
                        .setCancelable(true);


                AlertDialog alert = builder.create();
                alert.show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(FIASActivity.this);
                builder.setTitle("Ошибка")
                        .setMessage("Заполните все поля. " + comp.oblast + comp.city + comp.street + comp.streetNumber)
                        .setCancelable(true);


                AlertDialog alert = builder.create();
                alert.show();
            }

        });
    }


    @SuppressLint("SetTextI18n")
    private void setAdditionalInfoInUI() {
        final TextView textView = findViewById(R.id.textView4);
        String index = "--";

        if (comp.index != null) {
            index = comp.index;
        }

        if (index.equals("null")) {
            index = "--";
        }

        textView.setText("Индекс: " + index);

    }


}
