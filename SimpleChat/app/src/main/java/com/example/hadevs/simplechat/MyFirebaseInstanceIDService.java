package com.example.hadevs.simplechat;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Hadevs on 26.11.2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();

        if (MyStorage.currentUser == null) {
            return;
        }

        String token = FirebaseInstanceId.getInstance().getToken();
        System.out.println("Refreshed token: " + token);

        MyStorage.currentUser.token = token;
        MyStorage.refreshOnServer();
    }
}
