package com.example.hadevs.simplechat;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Hadevs on 09.11.2017.
 */
public class MyStorage {
    public static ArrayList<User> allUsers = new ArrayList<User>();
    public static User currentUser = null;
    public static User oponentUser = null;
    public static String oponentId;
    public static String token = "";


    public static void refreshOnServer() {

        if (currentUser == null) {
            return;
        }


        HashMap<String, String> data = new HashMap<String, String>();
        data.put("name", MyStorage. currentUser.name.replaceAll("\\s+",""));
        data.put("_id", MyStorage.currentUser.id);
        data.put("surname", MyStorage.currentUser.surname.replaceAll("\\s+",""));
        data.put("secondName", MyStorage.currentUser.secondName.replaceAll("\\s+",""));
        data.put("passNumber", MyStorage.currentUser.passNumber);
        data.put("placePasport", MyStorage.currentUser.placePasport);
        data.put("facePasport", MyStorage.currentUser.facePasport);
        data.put("timePasport", MyStorage.currentUser.timePasport);
        data.put("birthday", MyStorage.currentUser.birthday);
        data.put("INN", MyStorage.currentUser.INN);
        data.put("registration", MyStorage.currentUser.registration);
        data.put("token", MyStorage.currentUser.token);
        data.put("phoneNumber", MyStorage.currentUser.phoneNumber);
        data.put("bill", MyStorage.currentUser.bill);
        data.put("BIC", MyStorage.currentUser.BIC);
        data.put("bankName", MyStorage.currentUser.bankName);
        data.put("bankBill", MyStorage.currentUser.bankBill);
        data.put("bankCard", MyStorage.currentUser.bankCard);
        data.put("fiasIndex", MyStorage.currentUser.fiasIndex);
        data.put("fiasOblast", MyStorage.currentUser.fiasOblast);
        data.put("fiasCity", MyStorage.currentUser.fiasCity);
        data.put("fiasStreet", MyStorage.currentUser.fiasStreet);
        data.put("fiasStreetNumber", MyStorage.currentUser.fiasStreetNumber);
        data.put("fiasApartament", MyStorage.currentUser.fiasApartament);
        data.put("fiasStreetId", MyStorage.currentUser.fiasStreetId);
        data.put("fiasOblastId", MyStorage.currentUser.fiasOblastId);
        data.put("fiasCityId", MyStorage.currentUser.fiasCityId);
        data.put("codeSubdivision", MyStorage.currentUser.codeSubdivision);

        System.out.println("Out coming hashmap: " + data);

        AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);

        asyncHttpPost.setListener(new AsyncHttpPost.Listener(){
            @Override
            public void onResult(String result) {
                // do something, using return value from network

            }
        });

        asyncHttpPost.execute("http://85.143.214.81:27018/editUser");
    }
}