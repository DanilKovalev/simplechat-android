package com.example.hadevs.simplechat;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;

import static com.example.hadevs.simplechat.MainActivity.PREFS_NAME;

public class NotificationsActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        final Switch mySwitch = (Switch) findViewById(R.id.switch1);
//
        setTitle("Уведомления");
        loadSavedValue();
        mySwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                set(isChecked);
            }
        });

    }

    private void loadSavedValue() {
        final Switch mySwitch = (Switch) findViewById(R.id.switch1);

        SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        final String key = "notificationsEnabled";
        if (sharedPref.contains(key)) {
            final Boolean value = sharedPref.getBoolean(key, false);
            mySwitch.setChecked(value);
        } else {
            mySwitch.setChecked(true);
        }


    }

    private void set(Boolean value) {
        SharedPreferences sharedPref = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("notificationsEnabled", value);
        editor.commit();
    }
}
