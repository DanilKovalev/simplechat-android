package com.example.hadevs.simplechat;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Hadevs on 09.11.2017.
 */

@IgnoreExtraProperties
public class FirMessage {

    public String date;
    public String lastName;
    public String name;
    public String sender;
    public String surname;
    public String text;
    public String type;
    public String photoID;
    public String videoID;
    public String status;
    public String receiver;

    public FirMessage() {

    }

    public FirMessage(String date, String lastName, String name, String sender, String surname, String text, String type, String photoID, String videoID, String status, String receiver) {
        this.date = date;
        this.lastName = lastName;
        this.receiver = receiver;
        this.name = name;
        this.sender = sender;
        this.surname = surname;
        this.text = text;
        this.type = type;
        this.photoID = photoID;
        this.videoID = videoID;
        this.status = status;
    }
}