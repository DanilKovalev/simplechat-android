package com.example.hadevs.simplechat;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class ChangePasswordActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        final EditText oldField = findViewById(R.id.editText10);
        final EditText newField = findViewById(R.id.editText13);

        Button changeButton = findViewById(R.id.button6);
        changeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // request to server with password changing

                if (oldField.getText().toString().equals(MyStorage.currentUser.password)) {

                    HashMap<String, String> data = new HashMap<String, String>();
                    data.put("userId", MyStorage.currentUser.id.replaceAll("\\s+",""));
                    data.put("newPassword", newField.getText().toString());

                    System.out.println("Out coming hashmap: " + data);

                    AsyncHttpPost asyncHttpPost = new AsyncHttpPost(data);

                    asyncHttpPost.setListener(new AsyncHttpPost.Listener(){
                        @Override
                        public void onResult(String result) {
                            MyStorage.currentUser.password = newField.getText().toString();
                            // do something, using return value from network
                            AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this)
                                    .setMessage("Ваш пароль успешно изменен")
                                    .setNegativeButton("ОК",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {dialog.cancel();
                                                }
                                            });
                            AlertDialog alert = builder.create();
                            alert.show();
                        }
                    });

                    asyncHttpPost.execute("http://85.143.214.81:27018/changePassword");
                } else {

                    AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this)
                            .setMessage("Вы ввели неправильный старый пароль.")
                            .setNegativeButton("ОК",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }

            }
        });
    }
}
